module gitlab.com/IvanTurgenev/beerchatgo

go 1.15

require (
	github.com/bwmarrin/discordgo v0.23.2
	github.com/gofiber/fiber/v2 v2.5.0
)
