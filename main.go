package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"

	"github.com/bwmarrin/discordgo"
	"github.com/gofiber/fiber/v2"
)

// {"type":"discord","name":"Discord","username":"kmosq","channel":"main","message":"dfd"}
// [{"type":"discord","name":"Discord","username":"kmosq","channel":"main","message":"test"}]
// {"username":"kmosq","message":"bleak","type":"discord","name":"Discord","channel":"main"}
type message struct {
	PlayerName  string `json:"username"`
	Message     string `json:"message"`
	MessageType string `json:"type"`
	Name        string `json:"name"`
	Channel     string `json:"channel"`
}

// type messags []message

var (
	token       string
	channelName string
	channelID   string
	webtomod    = make(chan message, 50)
)

// This function will be called (due to AddHandler above) every time a new
// message is created on any channel that the authenticated bot has access to.
func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {

	// Ignore all messages created by the bot itself
	// This isn't required in this specific example but it's a good practice.
	if m.Author.ID == s.State.User.ID {
		return
	}

	if m.ChannelID == channelID {
		p := new(message)
		p.PlayerName = m.Author.Username
		p.Message = m.Content
		p.MessageType = "discord"
		p.Name = "Discord"
		p.Channel = "main"
		webtomod <- *p
	}

	// // If the message is "ping" reply with "Pong!"
	// if m.Content == "ping" {
	// 	s.ChannelMessageSend(m.ChannelID, "Pong!")
	// }

	// // If the message is "pong" reply with "Ping!"
	// if m.Content == "pong" {
	// 	s.ChannelMessageSend(m.ChannelID, "Ping!")
	// }
}

func searchChannel(dg *discordgo.Session) {
	// Loop through each guild in the session
	for _, guild := range dg.State.Guilds {

		// Get channels for this guild
		channels, _ := dg.GuildChannels(guild.ID)
		for _, c := range channels {
			// Check if channel is a guild text channel and not a voice or DM channel
			if c.Type != discordgo.ChannelTypeGuildText {
				continue
			}
			if c.Name == channelName {
				channelID = c.ID
				return
			}
		}
	}

}

func main() {
	token = os.Getenv("DISCORDBOTTOKEN")
	channelName = os.Getenv("DISCORDCHANNEL")
	port := os.Getenv("BEERCHATGOPORT")

	// Create a new Discord session using the provided bot token.
	dg, err := discordgo.New("Bot " + token)
	if err != nil {
		fmt.Println("error creating Discord session,", err)
		return
	}

	// Register the messageCreate func as a callback for MessageCreate events.
	dg.AddHandler(messageCreate)

	// In this example, we only care about receiving message events.
	dg.Identify.Intents = discordgo.IntentsGuildMessages

	// Open a websocket connection to Discord and begin listening.
	err = dg.Open()
	if err != nil {
		fmt.Println("error opening connection,", err)
		return
	}
	searchChannel(dg)
	dg.ChannelMessageSend(channelID, "**beerchatgo proxy connected to discord**")

	app := fiber.New()
	app.Post("/", func(c *fiber.Ctx) error {
		p := new(message)

		if err := c.BodyParser(p); err != nil {
			return err
		}

		if p.MessageType == "me" {
			dg.ChannelMessageSend(channelID, "* **"+p.PlayerName+"**"+" "+p.Message)
			// discord.Say("* **" + p.PlayerName + "**" + " " + p.Message)

		} else if p.PlayerName == "" {
			// discord.Say(p.Message)
			dg.ChannelMessageSend(channelID, p.Message)

		} else {
			dg.ChannelMessageSend(channelID, "**<"+p.PlayerName+">**"+" "+p.Message)
			// discord.Say("**<" + p.PlayerName + ">**" + " " + p.Message)

		}

		return c.SendString("Success")

	})

	// give response when at /
	app.Get("/", func(c *fiber.Ctx) error {
		select {
		case msg := <-webtomod:
			var tempmessages [1]message
			tempmessages[0] = msg
			return c.JSON(tempmessages)
		default:
			msg := new(message)
			return c.JSON(msg)
		}
	})

	c := make(chan os.Signal, 1)   // Create channel to signify a signal being sent
	signal.Notify(c, os.Interrupt) // When an interrupt is sent, notify the channel

	// Goroutine to monitor the channel and run app.Shutdown when an interrupt is recieved
	// This should cause app.Listen to return nil, then allowing the cleanup tasks to be
	// run.
	go func() {
		_ = <-c
		fmt.Println("Gracefully shutting down...")
		_ = app.Shutdown()
	}()

	if err := app.Listen(":" + port); err != nil {
		log.Panic(err)
	}

	dg.ChannelMessageSend(channelID, "**beerchatgo shutting down**")
	dg.Close()

}
